<?php
$config = [
	'base_url' => 'http://localhost/dmnt2/'
	, 'images_url' => 'http://localhost/idcard/assets/images/'
	, 'theme' => 'modern'
	, 'default_module' => 'user'
	, 'user_images_path' => 'assets/images/user/'
	, 'kartu_path' => 'assets/images/kartu/'
	, 'images_path' => 'assets/images/'
	, 'foto_path' => 'assets/images/foto/'
    , 'pembayaran' => 'assets/images/pembayaran/'
    , 'gambar_desain' => 'assets/images/gambar_desain/'
];