<?php


class dbException
{
	public function __construct($e, $text = '') {
		
		$title = 'Database Error';
		$content = '';
		if ($text) {
			$content .= '<p>' . $text . '</p>';
		}
		$content .= '<p><strong>Error</strong> : ' . $e->getMessage() . '</p>';
		include 'display_error.php';
		die;
	}
}

?>