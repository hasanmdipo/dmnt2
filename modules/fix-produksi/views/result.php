<div class="card">
	<div class="card-header">
		<h5 class="card-title"><?=$app_module['judul_module']?></h5>
	</div>
	
	<div class="card-body">
		<a href="?action=add" class="btn btn-success btn-xs"><i class="fa fa-plus pr-1"></i> Tambah Data</a>
		<hr/>
		<?php 
		if (!$result) {
			show_message('Data tidak ditemukan', 'error', false);
		} else {
			if (!empty($msg)) {
				show_alert($msg);
			}
			?>
			<div class="table-responsive">
			<table class="table table-striped table-bordered table-hover">
			<thead>
			<tr>
				<th>No</th>
				<th>Nama Tim</th>
				<th>Nama Bahan</th>
				<th>Kategori Bahan</th>
				<th>Nomer Kerah</th>
				<th>Total Baju</th>
				<th>Ukuran</th>
				<th>Notes</th>
				<th>Desain Fix</th>
				<th>Aksi</th>
			</tr>
			</thead>
			<tbody>
			<?php
			require_once('helpers/html.php');
			
			$i = 1;
			foreach ($result as $key => $val) {
				echo '<tr>
						<td>' . $i . '</td>
						<td>' . $val['nama_tim'] . '</td>
						<td>' . $val['alamat_bahan'] . '</td>
						<td>' . $val['kategori_bahan'] . '</td>
						<td>' . $val['nomer_kerah'] . '</td>
						<td>' . $val['total_baju'] . '</td>
						<td>' . $val['ukuran'] . '</td>
						<td>' . $val['notes'] . '</td>
						<td>' . $val['desain_fix'] . '</td>
						<td>'. btn_action([
									'edit' => ['url' => '?action=edit&id='. $val['id']]
								, 'delete' => ['url' => ''
												, 'id' =>  $val['id']
												, 'delete-title' => 'Hapus Data Pesanann: <strong>'.$val['nama_lengkap'].'</strong> ?'
											]
								, 'print' => ['url' => '?action=print&id='. $val['id']
												, 'btn_class' => 'btn-primary'
												, 'icon' => 'fa fa-print'
												, 'text' => 'Detail'
											]
							]) .
						'</td>
					</tr>';
					$i++;
			}
			?>
			</tbody>
			</table>
			</div>
			<?php 
		} ?>
	</div>
</div>