<div class="card">
	<div class="card-header">
		<h5 class="card-title"><?=$title?></h5>
	</div>
	
	<div class="card-body">
		<?php 
			include 'helpers/html.php';
			echo btn_label(['class' => 'btn btn-success btn-xs',
				'url' => module_url() . '?action=add',
				'icon' => 'fa fa-plus',
				'label' => 'Tambah Data'
			]);
			
			echo btn_label(['class' => 'btn btn-light btn-xs',
				'url' => module_url(),
				'icon' => 'fa fa-arrow-circle-left',
				'label' => $app_module['judul_module']
			]);
		?>
		<hr/>
		<form method="post" action="" class="form-horizontal" enctype="multipart/form-data">
			<div class="tab-content" id="myTabContent">
				<div class="form-group row">
					<label class="col-sm-3 col-md-2 col-lg-3 col-xl-2 col-form-label">ID</label>
					<div class="col-sm-5">
						<input class="form-control" type="text" name="id_pesanan" value="<?=set_value('id_pesanan', @$id_pesanan)?>" required="required"/>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-3 col-md-2 col-lg-3 col-xl-2 col-form-label">Nama Tim</label>
					<div class="col-sm-5">
						<input class="form-control" type="text" name="nama_tim" value="<?=set_value('nama_tim', @$nama_tim)?>" required="required"/>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-3 col-md-2 col-lg-3 col-xl-2 col-form-label">Kategori Paket</label>
					<div class="col-sm-5">
						<input class="form-control" type="text" name="kategori_paket" value="<?=set_value('kategori_paket', @$kategori_paket)?>" required="required"/>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-3 col-md-2 col-lg-3 col-xl-2 col-form-label">Nama Bahan</label>
					<div class="col-sm-5">
						<input class="form-control" type="text" name="nama_bahan" value="<?=set_value('nama_bahan', @$nama_bahan)?>"/>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-3 col-md-2 col-lg-3 col-xl-2 col-form-label">Nomer Kerah</label>
					<div class="col-sm-5">
						<input class="form-control" type="text" name="nomer_kerah" value="<?=set_value('nomer_kerah', @$nomer_kerah)?>" required="required"/>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-3 col-md-2 col-lg-3 col-xl-2 col-form-label">Notes</label>
					<div class="col-sm-5">
						<input class="form-control" type="text" name="notes" value="<?=set_value('notes', @$notes)?>" required="required"/>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-3 col-md-2 col-lg-3 col-xl-2 col-form-label">Ukuran Baju</label>
					<div class="col-sm-5">
						<input class="form-control" type="text" name="ukuran" value="<?=set_value('ukuran', @$ukuran)?>" required="required"/>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-3 col-md-2 col-lg-3 col-xl-2 col-form-label">Total Baju</label>
					<div class="col-sm-5">
						<input class="form-control" type="text" name="total_baju" value="<?=set_value('total_baju', @$total_baju)?>" required="required"/>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-3 col-md-2 col-lg-3 col-xl-2 col-form-label">Upload Gambar</label>
					<div class="col-sm-5">
						<?php 
						$file_background_belakang = @$background_belakang;
						if (!empty($file_background_belakang) && file_exists($config['kartu_path'] . $file_background_belakang))
						echo '<div class="list-foto" style="margin:inherit;margin-bottom:10px"><img src="'.BASE_URL. $config['kartu_path'] . $file_background_belakang . '"/></div>';
						
						?>
						<input type="file" class="file" name="desain_fix">
							<?php if (!empty($form_errors['desain_fix'])) echo '<small class="alert alert-danger">' . $form_errors['desain_fix'] . '</small>'?>
							<small class="small" style="display:block">Maksimal 300Kb, Minimal 100px x 100px, Tipe file: .JPG, .JPEG, .PNG</small>
						<div class="upload-img-thumb"><span class="img-prop"></span></div>
					</div>
				</div>
				<div class="form-group row mb-0">
					<div class="col-sm-5">
						<button type="submit" name="submit" value="submit" class="btn btn-primary">Submit</button>
						<input type="hidden" name="id" value="<?=@$_GET['id']?>"/>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>