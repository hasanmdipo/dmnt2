<div class="card">
    <div class="card-header">
        <h5 class="card-title"><?= $title ?></h5>
    </div>

    <div class="card-body">
        <?php
        include 'helpers/html.php';
        echo btn_label(['class' => 'btn btn-success btn-xs',
            'url' => module_url() . '?action=add',
            'icon' => 'fa fa-plus',
            'label' => 'Tambah Data'
        ]);

        echo btn_label(['class' => 'btn btn-light btn-xs',
            'url' => module_url(),
            'icon' => 'fa fa-arrow-circle-left',
            'label' => $app_module['judul_module']
        ]);
        ?>
        <hr/>
        <form method="post" action="" class="form-horizontal" enctype="multipart/form-data">
            <div class="tab-content" id="myTabContent">
                <?php
                if ($_GET['action'] === 'add') {
                    ?>
                    <div class="form-group row">
                        <label class="col-sm-3 col-md-2 col-lg-3 col-xl-2 col-form-label">ID Pesanan</label>
                        <div class="col-sm-5">
                            <select class="form-control" name="id_pesanan" id="id_pesanan">
                                <?php
                                if ($_GET['action'] === 'add') {
                                    foreach ($data['result'] as $row) {
                                        ?>
                                        <option value="<?= $row['id_pesanan']; ?>"><?= $row['id_pesanan']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <?php
                }
                ?>
                <div class="form-group row">
                    <label class="col-sm-3 col-md-2 col-lg-3 col-xl-2 col-form-label">ID Pembayaran</label>
                    <div class="col-sm-5">
                        <input type="number" class="form-control" name="id_pembayaran" id="id_pembayaran" required
                               value="<?= set_value('id_pembayaran', @$id_pembayaran) ?>">
                    </div>
                </div>
                <!--<div class="form-group row">
                    <label class="col-sm-3 col-md-2 col-lg-3 col-xl-2 col-form-label">Alamat Lengkap</label>
                    <div class="col-sm-5">
                        <textarea name="alamat_lengkap" class="form-control"><? /*=set_value('alamat_lengkap', @$alamat_lengkap)*/ ?></textarea>
                    </div>
                </div>-->
                <div class="form-group row">
                    <label class="col-sm-3 col-md-2 col-lg-3 col-xl-2 col-form-label">Total Harga Pesanan</label>
                    <div class="col-sm-5">
                        <input class="form-control" type="text" name="total_pesanan"
                               value="<?= set_value('harga_pesanan', @$harga_pesanan) ?>" required="required"/>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-md-2 col-lg-3 col-xl-2 col-form-label">Total Ongkir</label>
                    <div class="col-sm-5">
                        <input class="form-control" type="text" name="total_ongkir"
                               value="<?= set_value('total_ongkir', @$ongkir) ?>"/>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-md-2 col-lg-3 col-xl-2 col-form-label">Total Pembayaran</label>
                    <div class="col-sm-5">
                        <input class="form-control" type="text" name="total_pembayaran"
                               value="<?= set_value('total_pembayaran', @$pembayaran) ?>" required="required"/>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-md-2 col-lg-3 col-xl-2 col-form-label">Upload Bukti Pembayaran</label>
                    <div class="col-sm-5">
                        <div class="custom-file">
                            <input type="file" name="bukti_pembayaran" id="bukti_pembayaran"
                                   accept="image/png,image/jpg,image/jpeg"
                                   value="<?= set_value('bukti_pembayaran', @$bukti_pembayaran) ?>"
                                   required
                            />
                            <label class="custom-file-label" for="bukti_pembayaran"
                                   id="label_bukti_pembayaran"></label>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-md-2 col-lg-3 col-xl-2 col-form-label">Kategori Paket</label>
                    <div class="col-sm-5">
                        <select class="form-control" name="kategori_paket" id="kategori_paket">
                            <option value="DP">DP</option>
                            <option value="LUNAS">LUNAS</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row mb-0">
                    <div class="col-sm-5">
                        <button type="submit" name="submit" value="submit" class="btn btn-primary">Submit</button>
                        <input type="hidden" name="id" value="<?= @$_GET['id'] ?>"/>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    $('#bukti_pembayaran').on('change', function () {
        let fileName = event.target.files[0].name;
        $(this).next('#label_bukti_pembayaran').html(fileName);
        $(this).next('#label_bukti_pembayaran').css("overflow", "hidden");
        $(this).next('#label_bukti_pembayaran').css("white-space", "nowrap");
        $(this).next('#label_bukti_pembayaran').css("text-overflow", "ellipsis");
    });
</script>