<div class="card">
	<div class="card-header">
		<h5 class="card-title">Detail Pesanan</h5>
	</div>
	
	<div class="card-body">
		<?php 
		if (!$data) {
			show_message('Data tidak ditemukan', 'error', false);
		} else {
			if (!empty($msg)) {
				show_alert($msg);
			}
			?>
			<p>ID Pembayaran : <?= $data['id_pembayaran']; ?></p>
			<p>Total Harga Pesanan : <?= $data['harga_pesanan']; ?></p>
			<p>Total Ongkir : <?= $data['ongkir']; ?></p>
			<p>Total Pembayaran : <?= $data['pembayaran']; ?></p>
            <p>Bukti Pembayaran : <a href="<?= $data['bukti_pembayaran']; ?>" target="_blank">Lihat Bukti Pembayaran</a></p>
			<p>Status : <?= $data['status']; ?></p>
			<?php
		} ?>

		<div class="row">
			<div class="col text-center">
				<?php 
					include 'helpers/html.php';
					
					echo btn_label(['class' => 'btn btn-primary btn-xs',
						'url' => module_url(),
						'icon' => 'fa fa-arrow-circle-left',
						'label' => 'Kembali'
					]);
				?>
			</div>
		</div>
	</div>
</div>