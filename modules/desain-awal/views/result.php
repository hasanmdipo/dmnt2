<div class="card">
	<div class="card-header">
		<h5 class="card-title"><?=$app_module['judul_module']?></h5>
	</div>
	
	<div class="card-body">
		<hr/>
		<?php 
		if (!$result) {
			show_message('Data tidak ditemukan', 'error', false);
		} else {
			if (!empty($msg)) {
				show_alert($msg);
			}
			?>
			<div class="table-responsive">
			<table class="table table-striped table-bordered table-hover">
			<thead>
			<tr>
				<th>No</th>
				<th>ID</th>
				<th>Gambar Desain</th>
				<th>Aksi</th>
			</tr>
			</thead>
			<tbody>
			<?php
			require_once('helpers/html.php');
			
			$i = 1;
			foreach ($result as $key => $val) {
				echo '<tr>
						<td>' . $i . '</td>
						<td>' . $val['nama_lengkap'] . '</td>
						<td>' . $val['detail_pesanan'] . '</td>
						<td>'. btn_action([
									'edit' => ['url' => '?action=edit&id='. $val['id']]
								, 'delete' => ['url' => ''
												, 'id' =>  $val['id']
												, 'delete-title' => 'Hapus Data Desain Awal: <strong>'.$val['nama_lengkap'].'</strong> ?'
											]
								, 'print' => ['url' => '?action=print&id[]='. $val['id']
												, 'btn_class' => 'btn-primary'
												, 'icon' => 'fa fa-print'
												, 'text' => 'Detail'
												, 'attr' => ['target' => '_blank']
											]
							]) .
						'</td>
					</tr>';
					$i++;
			}
			?>
			</tbody>
			</table>
			</div>
			<?php 
		} ?>
	</div>
</div>