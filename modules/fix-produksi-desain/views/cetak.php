<div class="card">
	<div class="card-header">
		<h5 class="card-title">Detail Pesanan</h5>
	</div>
	
	<div class="card-body">
		<?php 
		if (!$data) {
			show_message('Data tidak ditemukan', 'error', false);
		} else {
			if (!empty($msg)) {
				show_alert($msg);
			}
			?>
			<p>ID : <?= $data['id']; ?></p>
			<p>Nama Tim : <?= $data['nama_tim']; ?></p>
			<p>Kategori Paket : <?= $data['kategori_paket']; ?></p>
			<p>Nama Bahan : <?= $data['nama_bahan']; ?></p>
			<p>Nomer Kerah : <?= $data['nomer_kerah']; ?></p>
			<p>Notes : <?= $data['notes']; ?></p>
			<p>Ukuran Baju : <?= $data['ukuran_baju']; ?></p>
			<p>Total Baju : <?= $data['total_baju']; ?></p>
			<p>Upload Gambar : <?= $data['upload_gambar']; ?></p>
			<?php
		} ?>

		<div class="row">
			<div class="col text-center">
				<?php 
					include 'helpers/html.php';
					
					echo btn_label(['class' => 'btn btn-primary btn-xs',
						'url' => module_url(),
						'icon' => 'fa fa-arrow-circle-left',
						'label' => 'Kembali'
					]);
				?>
			</div>
		</div>
	</div>
</div>