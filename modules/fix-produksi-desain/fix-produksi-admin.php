<?php

login_required();
$js[] = BASE_URL . 'assets/vendors/bootstrap-datepicker/js/bootstrap-datepicker.js';
$js[] = THEME_URL . 'assets/js/date-picker.js';
$styles[] = BASE_URL . 'assets/vendors/bootstrap-datepicker/css/bootstrap-datepicker3.css';

switch ($_GET['action']) 
{
	default: 
		action_notfound();
		
	// INDEX 
	case 'index':
		cek_hakakses('read_data');
		
		if (!empty($_POST['delete'])) 
		{
			cek_action('delete_data');
			$result = $db->delete('fix_produksi', ['id' => $_POST['id']]);
			// $result = true;
			if ($result) {
				$data['msg'] = ['status' => 'ok', 'message' => 'Data berhasil dihapus'];
			} else {
				$data['msg'] = ['status' => 'error', 'message' => 'Data gagal dihapus'];
			}
		}
		
		$where = '';
		if ($list_action['read_data'] == 'own') {
			$where = ' WHERE id_user_input = ' . $_SESSION['user']['id_user'];
		}
		$sql = 'SELECT * FROM fix_produksi' . $where;
		$data['result'] = $db->query($sql)->result();
		
		load_view('views/result.php', $data);
		
	case 'add':
		$data['title'] = 'Tambah ' . $app_module['judul_module'];
		
		// Submit
		$data['msg'] = [];
		$query = false;
		if (isset($_POST['submit'])) {			
			$data_db = set_data();
			$query = $db->insert('fix_produksi', $data_db);
			if ($query) {
				$newid = $db->lastInsertId();
				$data['msg']['status'] = 'ok';
				$data['msg']['content'] = 'Data berhasil disimpan';

				header('Location: http://localhost/dmnt2/fix_produksi');
			} else {
				$data['msg']['status'] = 'error';
				$data['msg']['content'] = 'Data gagal disimpan';
			}
		} 	

		load_view('views/form.php', $data);
		
	case 'edit': 
		cek_hakakses('update_data');
		cek_action('update_data');

		$sql = 'SELECT * FROM fix_produksi WHERE id = ?';
		$result = $db->query($sql, trim($_GET['id']))->result();
		$data = $result[0];
		
		$breadcrumb['Add'] = '';

		$data['title'] = 'Edit ' . $app_module['judul_module'];
		
		// Submit
		$data['msg'] = [];
		if (isset($_POST['submit'])) 
		{
			
			$form_errors = validate_form();
			
			$sql = 'SELECT * FROM fix_produksi WHERE id = ?';
			$img_db = $db->query($sql, $_POST['id'])->row();
			
			if ($form_errors) {
				$data['msg']['status'] = 'error';
				$data['msg']['content'] = $form_errors;
			} else {
				
				$data_db = set_data();
				$data_db['tgl_edit'] = date('Y-m-d');
				$data_db['id_user_edit'] = $_SESSION['user']['id_user'];
				
				$query = false;
	
				$query = $db->update('fix_produksi', $data_db, 'id = ' . $_POST['id']);
				if ($query) {
					$data['msg']['status'] = 'ok';
					$data['msg']['content'] = 'Data berhasil disimpan';

					header('Location: http://localhost/dmnt2/fix_produksi');
				} else {
					$data['msg']['status'] = 'error';
					$data['msg']['content'] = 'Data gagal disimpan';
				}
			}
		}

		load_view('views/form.php', $data);

	case 'print':
		$sql = 'SELECT * FROM fix_produksi WHERE id = ?';
		$result = $db->query($sql, trim($_GET['id']))->result();
		$data = $result[0];

		load_view('views/cetak.php', $data);
}

function cek_action($action) 
{
	global $list_action;
	global $db;
	
	$sql = 'SELECT * FROM fix_produksi WHERE id = ?';
	$result = $db->query($sql, trim($_REQUEST['id']))->result();
	$data = $result[0];
	// echo '<pre>'; print_r ($list_action); die;
	if ($list_action[$action] == 'own') {
		if ($data['id_user_input'] != $_SESSION['user']['id_user']) {
			echo 'Anda tidak diperkenankan mengakses halaman ini';
			die;
		}
	}
}

function set_data() {
	$data_db['id'] = $_POST['id'];
	$data_db['nama_tim'] = $_POST['nama_tim'];
	$data_db['nama_bahan'] = $_POST['nama_bahan'];
	$data_db['kategori_paket'] = $_POST['kategori_paket'];
	$data_db['nomer_kerah'] = $_POST['nomer_kerah'];
	$data_db['total_baju'] = $_POST['total_baju'];
	$data_db['ukuran'] = $_POST['ukuran'];
	$data_db['notes'] = $_POST['notes'];
	$data_db['desain_fix'] = $_POST['desain_fix'];
	return $data_db;
}

function validate_form() {
	
	require_once('libraries/form_validation.php');
	$validation = new FormValidation();
	$validation->setRules('id', 'ID', 'required');
	$validation->setRules('nama_tim', 'Nama Tim', 'required');
	$validation->setRules('nama_bahan', 'Nama Bahan', 'trim|required');
	$validation->setRules('kategori_paket', 'Kategori Paket', 'trim|required');
	$validation->setRules('nomer_kerah', 'Nomer Kerah', 'trim|required');
	$validation->setRules('total_baju', 'Total Baju', 'trim|required');
	$validation->setRules('ukuran', 'Ukuran', 'trim|required');
	$validation->setRules('notes', 'Notes', 'trim|required');
	$validation->setRules('desain_fix', 'Desain Fix', 'trim|required');
	
	$validation->validate();
	$form_errors =  $validation->getMessage();
	
	return $form_errors;
} 