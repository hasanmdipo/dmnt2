<?php

login_required();
$js[] = BASE_URL . 'assets/vendors/bootstrap-datepicker/js/bootstrap-datepicker.js';
$js[] = THEME_URL . 'assets/js/date-picker.js';
$styles[] = BASE_URL . 'assets/vendors/bootstrap-datepicker/css/bootstrap-datepicker3.css';

switch ($_GET['action']) {
    default:
        action_notfound();

    // INDEX
    case 'index':
        cek_hakakses('read_data');

        if (!empty($_POST['delete'])) {
            cek_action('delete_data');
            $result = $db->delete('`hasil-printing`', ['id' => $_POST['id']]);
            // $result = true;
            if ($result) {
                $data['msg'] = ['status' => 'ok', 'message' => 'Data berhasil dihapus'];
            } else {
                $data['msg'] = ['status' => 'error', 'message' => 'Data gagal dihapus'];
            }
        }

        $where = '';
        if ($list_action['read_data'] == 'own') {
            $where = ' WHERE id_user_input = ' . $_SESSION['user']['id_user'];
        }
        $sql = 'SELECT * FROM `hasil-printing`' . $where;
        $data['result'] = $db->query($sql)->result();

        $sql = 'SELECT * FROM role WHERE id_role = ?';
        $data['role'] = $db->query($sql, trim($_SESSION['user']['id_role']))->result();

        load_view('views/result.php', $data);

    case 'add':
        $data['title'] = 'Tambah ' . $app_module['judul_module'];

        $sql = 'SELECT * FROM role WHERE id_role = ?';
        $data['role'] = $db->query($sql, trim($_SESSION['user']['id_role']))->result();

        // Submit
        $query = false;
        if (isset($_POST['submit'])) {
            $data_db = set_data($data['role']);

            $upload = upload_file($config['gambar_desain'], $data_db['gambar_desain']);
            $data_db['gambar_desain'] = $config['gambar_desain'] . $upload;
            $query = $db->insert('`hasil-printing`', $data_db);
            if ($query) {
                $data['msg']['status'] = 'ok';
                $data['msg']['content'] = 'Data berhasil disimpan';

                header('Location: http://localhost/dmnt2/hasil-printing');
            } else {
                $data['msg']['status'] = 'error';
                $data['msg']['content'] = 'Data gagal disimpan';
            }
        }

        load_view('views/form.php', $data);
    case 'edit':
        cek_hakakses('update_data');
        cek_action('update_data');

        $sql = 'SELECT * FROM `hasil-printing` WHERE id = ?';
        $result = $db->query($sql, trim($_GET['id']))->result();
        $data = $result[0];

        $breadcrumb['Add'] = '';

        $data['title'] = 'Edit ' . $app_module['judul_module'];

        $sql = 'SELECT * FROM role WHERE id_role = ?';
        $data['role'] = $db->query($sql, trim($_SESSION['user']['id_role']))->result();

        // Submit
        $data['msg'] = [];
        if (isset($_POST['submit'])) {
            $data_db = set_data($data['role']);
            $upload = upload_file($config['gambar_desain'], $data_db['gambar_desain']);
            $data_db['gambar_desain'] = $config['gambar_desain'] . $upload;

            $query = $db->update('`hasil-printing`', $data_db, 'id_pelanggan = ' . $_POST['id_pelanggan']);
            if ($query) {
                $data['msg']['status'] = 'ok';
                $data['msg']['content'] = 'Data berhasil disimpan';

                header('Location: http://localhost/dmnt2/hasil-printing');
            } else {
                $data['msg']['status'] = 'error';
                $data['msg']['content'] = 'Data gagal disimpan';
            }
        }

        load_view('views/form.php', $data);
    case 'print':
        $sql = 'SELECT * FROM `hasil-printing` WHERE id = ?';
        $result = $db->query($sql, trim($_GET['id']))->result();
        $data = $result[0];
        load_view('views/cetak.php', $data);
    case 'terima':
        cek_hakakses('update_data');
        cek_action('update_data');

        $id = $_GET['id'];
        $data_db['status'] = 'terima';

        $query = $db->update('`hasil-printing`', $data_db, 'id = ' . $id);
        if ($query) {
            $data['msg']['status'] = 'ok';
            $data['msg']['content'] = 'Data berhasil disimpan';

            header('Location: http://localhost/dmnt2/hasil-printing');
        } else {
            $data['msg']['status'] = 'error';
            $data['msg']['content'] = 'Data gagal disimpan';
        }
    case 'tolak':
        cek_hakakses('update_data');
        cek_action('update_data');

        $sql = 'SELECT * FROM role WHERE id_role = ?';
        $data['role'] = $db->query($sql, trim($_SESSION['user']['id_role']))->result();

        // Submit
        $data['msg'] = [];
        if (isset($_POST['submit'])) {
            $data_db['komentar'] = $_POST['komentar'];
            $data_db['status']   = 'tolak';

            $query = $db->update('`hasil-printing`', $data_db, 'id = ' . $_POST['id']);
            if ($query) {
                $data['msg']['status'] = 'ok';
                $data['msg']['content'] = 'Data berhasil disimpan';

                header('Location: http://localhost/dmnt2/hasil-printing');
            } else {
                $data['msg']['status'] = 'error';
                $data['msg']['content'] = 'Data gagal disimpan';
            }
        }
}

function cek_action($action)
{
    global $list_action;
    global $db;

    $sql = 'SELECT * FROM `hasil-printing` WHERE id = ?';
    $result = $db->query($sql, trim($_REQUEST['id']))->result();
    $data = $result[0];
    // echo '<pre>'; print_r ($list_action); die;
    if ($list_action[$action] == 'own') {
        if ($data['id_user_input'] != $_SESSION['user']['id_user']) {
            echo 'Anda tidak diperkenankan mengakses halaman ini';
            die;
        }
    }
}

function set_data($role)
{
    $data_db['id_pelanggan'] = $_POST['id_pelanggan'];
    $data_db['gambar_desain'] = $_FILES['gambar_desain'];
    $data_db['komentar'] = $role[0]['nama_role'] === 'admin' ?  $_POST['komentar'] : '';
    $data_db['status'] = $role[0]['nama_role'] === 'admin' ? $_POST['status'] : '';

    return $data_db;
}

function validate_form()
{

    require_once('libraries/form_validation.php');
    $validation = new FormValidation();
    $validation->setRules('id_pembayaran', 'ID', 'required');
    $validation->setRules('total_pesanan', 'Total Harga Pesanan', 'required');
    $validation->setRules('total_ongkir', 'Total Ongkir', 'trim|required');
    $validation->setRules('total_pembayaran', 'Total Pembayaran', 'trim|required');
    $validation->setRules('bukti_pembayaran', 'Upload Bukti Pembayaran', 'required');
    $validation->setRules('kategori_paket', 'Kategori Paket', 'required');

    $validation->validate();
    $form_errors = $validation->getMessage();

    return $form_errors;
}