<div class="card">
    <div class="card-header">
        <h5 class="card-title"><?= $app_module['judul_module'] ?></h5>
    </div>

    <div class="card-body">
        <a href="?action=add" class="btn btn-success btn-xs"><i class="fa fa-plus pr-1"></i> Tambah Data</a>
        <hr/>
        <?php
        if (!$result) {
            show_message('Data tidak ditemukan', 'error', false);
        } else {
            if (!empty($data['msg'])) {
                show_alert($data['msg']);
            }
            ?>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>ID</th>
                        <th>Gambar Desain</th>
                        <th>Aksi</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    require_once('helpers/html.php');

                    $i = 1;
                    foreach ($result as $key => $val) {
                        echo '<tr>
						<td>' . $i . '</td>
						<td>' . $val['id_pesanan'] . '</td>
						<td><a href="' . $val['gambar_desain'] . '" target="_blank">Lihat Gambar Desain</a></td>
						<td>'. btn_action([
                            'edit' => ['url' => '?action=edit&id='. $val['id']]
                        , 'delete' => ['url' => ''
                                        , 'id' =>  $val['id']
                                        , 'delete-title' => 'Hapus Data Pembayaran: <strong>'.$val['id_pesanan'].'</strong> ?'
                                    ]
                        , 'print' => ['url' => '?action=print&id='. $val['id']
                                        , 'btn_class' => 'btn-primary'
                                        , 'icon' => 'fa fa-print'
                                        , 'text' => 'Detail'
                                    ]
                    ]) .
                '</td>
					</tr>';
                        $i++;
                    }
                    ?>
                    </tbody>
                </table>
            </div>
            <?php
        } ?>
    </div>
</div>