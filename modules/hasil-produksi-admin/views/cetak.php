<div class="card">
	<div class="card-header">
		<h5 class="card-title">Detail Pesanan</h5>
	</div>
	
	<div class="card-body">
		<?php 
		if (!$data) {
			show_message('Data tidak ditemukan', 'error', false);
		} else {
			if (!empty($msg)) {
				show_alert($msg);
			}
			?>
			<p>ID : <?= $data['id_pesanan']; ?></p>
            <p>Gambar Desain : <a href="<?= $data['gambar_desain']; ?>" target="_blank">Lihat Gambar Desain</a></p>
			<?php
		} ?>

		<div class="row">
			<div class="col text-center">
				<?php 
					include 'helpers/html.php';
					
					echo btn_label(['class' => 'btn btn-primary btn-xs',
						'url' => module_url(),
						'icon' => 'fa fa-arrow-circle-left',
						'label' => 'Kembali'
					]);
				?>
			</div>
		</div>
	</div>
</div>