<div class="card">
    <div class="card-header">
        <h5 class="card-title"><?= $title ?></h5>
    </div>

    <div class="card-body">
        <?php
        include 'helpers/html.php';
        echo btn_label(['class' => 'btn btn-success btn-xs',
            'url' => module_url() . '?action=add',
            'icon' => 'fa fa-plus',
            'label' => 'Tambah Data'
        ]);

        echo btn_label(['class' => 'btn btn-light btn-xs',
            'url' => module_url(),
            'icon' => 'fa fa-arrow-circle-left',
            'label' => $app_module['judul_module']
        ]);
        ?>
        <hr/>
        <form method="post" action="" class="form-horizontal" enctype="multipart/form-data">
            <div class="tab-content" id="myTabContent">
                <div class="form-group row">
                    <label class="col-sm-3 col-md-2 col-lg-3 col-xl-2 col-form-label">ID </label>
                    <div class="col-sm-5">
                        <input type="number" class="form-control" name="id_pesanan" id="id_pesanan" required
                               value="<?= set_value('id_pesanan', @$id_pesanan) ?>">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-md-2 col-lg-3 col-xl-2 col-form-label">Upload Gambar Desain</label>
                    <div class="col-sm-5">
                        <div class="custom-file">
                            <input type="file" name="gambar_desain" id="gambar_desain"
                                   accept="image/png,image/jpg,image/jpeg"
                                   value="<?= set_value('gambar_desain', @$gambar_desain) ?>"
                                   required
                            />
                            <label class="custom-file-label" for="gambar_desain"
                                   id="label_gambar_desain"></label>
                        </div>
                    </div>
                </div>
                <?php
                if ($_GET['action'] === 'add' && $data['role'][0]['nama_role'] === 'admin') {
                    ?>
                    <div class="form-group row">
                        <label class="col-sm-3 col-md-2 col-lg-3 col-xl-2 col-form-label">ID Pelanggan</label>
                        <div class="col-sm-5">
                            <input type="number" class="form-control" name="id_pesanan" id="id_pesanan" required
                                   value="<?= set_value('id_pesanan', @$id_pesanan) ?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-md-2 col-lg-3 col-xl-2 col-form-label">ID Pelanggan</label>
                        <div class="col-sm-5">
                            <input type="number" class="form-control" name="id_pesanan" id="id_pesanan" required
                                   value="<?= set_value('id_pesanan', @$id_pesanan) ?>">
                        </div>
                    </div>
                    <?php
                }
                ?>
                <div class="form-group row mb-0">
                    <div class="col-sm-5">
                        <button type="submit" name="submit" value="submit" class="btn btn-primary">Submit</button>
                        <input type="hidden" name="id" value="<?= @$_GET['id'] ?>"/>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    $('#gambar_desain').on('change', function () {
        let fileName = event.target.files[0].name;
        $(this).next('#label_gambar_desain').html(fileName);
        $(this).next('#label_gambar_desain').css("overflow", "hidden");
        $(this).next('#label_gambar_desain').css("white-space", "nowrap");
        $(this).next('#label_gambar_desain').css("text-overflow", "ellipsis");
    });
</script>