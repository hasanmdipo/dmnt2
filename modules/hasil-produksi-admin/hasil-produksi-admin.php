<?php

login_required();
$js[] = BASE_URL . 'assets/vendors/bootstrap-datepicker/js/bootstrap-datepicker.js';
$js[] = THEME_URL . 'assets/js/date-picker.js';
$styles[] = BASE_URL . 'assets/vendors/bootstrap-datepicker/css/bootstrap-datepicker3.css';

switch ($_GET['action']) {
    default:
        action_notfound();

    // INDEX
    case 'index':
        cek_hakakses('read_data');

        if (!empty($_POST['delete'])) {
            cek_action('delete_data');
            $result = $db->delete('`hasil-produksi-admin`', ['id' => $_POST['id']]);
            // $result = true;
            if ($result) {
                $data['msg'] = ['status' => 'ok', 'message' => 'Data berhasil dihapus'];
            } else {
                $data['msg'] = ['status' => 'error', 'message' => 'Data gagal dihapus'];
            }
        }

        $where = '';
        if ($list_action['read_data'] == 'own') {
            $where = ' WHERE id_user_input = ' . $_SESSION['user']['id_user'];
        }
        $sql = 'SELECT * FROM `hasil-printing`' . $where;
        $data['result'] = $db->query($sql)->result();

        $sql = 'SELECT * FROM role WHERE id_role = ?';
        $data['role'] = $db->query($sql, trim($_SESSION['user']['id_role']))->result();

        load_view('views/result.php', $data);

    case 'add':
        $data['title'] = 'Tambah ' . $app_module['judul_module'];

        $sql = 'SELECT * FROM role WHERE id_role = ?';
        $data['role'] = $db->query($sql, trim($_SESSION['user']['id_role']))->result();

        // Submit
        $query = false;
        if (isset($_POST['submit'])) {
            $data_db = set_data($data['role']);

            $upload = upload_file($config['gambar_desain'], $data_db['gambar_desain']);
            $data_db['gambar_desain'] = $config['gambar_desain'] . $upload;
            $query = $db->insert('`hasil-produksi-admin`', $data_db);
            if ($query) {
                $data['msg']['status'] = 'ok';
                $data['msg']['content'] = 'Data berhasil disimpan';

                header('Location: http://localhost/dmnt2/hasil-produksi-admin');
            } else {
                $data['msg']['status'] = 'error';
                $data['msg']['content'] = 'Data gagal disimpan';
            }
        }

        load_view('views/form.php', $data);
    case 'edit':
        cek_hakakses('update_data');
        cek_action('update_data');

        $sql = 'SELECT * FROM `hasil-produksi-admin` WHERE id = ?';
        $result = $db->query($sql, trim($_GET['id']))->result();
        $data = $result[0];

        $breadcrumb['Add'] = '';

        $data['title'] = 'Edit ' . $app_module['judul_module'];

        $sql = 'SELECT * FROM role WHERE id_role = ?';
        $data['role'] = $db->query($sql, trim($_SESSION['user']['id_role']))->result();

        // Submit
        $data['msg'] = [];
        if (isset($_POST['submit'])) {
            $data_db = set_data($data['role']);
            $upload = upload_file($config['gambar_desain'], $data_db['gambar_desain']);
            $data_db['gambar_desain'] = $config['gambar_desain'] . $upload;

            $query = $db->update('`hasil-produksi-admin`', $data_db, 'id_pesanan = ' . $_POST['id_pesanan']);
            if ($query) {
                $data['msg']['status'] = 'ok';
                $data['msg']['content'] = 'Data berhasil disimpan';

                header('Location: http://localhost/dmnt2/hasil-produksi-admin');
            } else {
                $data['msg']['status'] = 'error';
                $data['msg']['content'] = 'Data gagal disimpan';
            }
        }
    case 'edit':
            cek_hakakses('update_data');
            cek_action('update_data');
    
            $sql = 'SELECT * FROM `hasil-produksi-admin` WHERE id = ?';
            $result = $db->query($sql, trim($_GET['id']))->result();
            $data = $result[0];
    
            $breadcrumb['Add'] = '';
    
            $data['title'] = 'Edit ' . $app_module['judul_module'];
    
            // Submit
            $data['msg'] = [];
            if (isset($_POST['submit'])) {
                $data_db = set_data();
                $upload = upload_file($config['hasil-produksi-admin'], $data_db['hasil-produksi-admin']);
                $data_db['gambar_desain'] = $config['hasil-produksi-admin'] . $upload;
    
                $query = $db->update('hasil-produksi-admin', $data_db, 'id_pesanan = ' . $_POST['id_pesanan']);
                if ($query) {
                    $data['msg']['status'] = 'ok';
                    $data['msg']['content'] = 'Data berhasil disimpan';
    
                    header('Location: http://localhost/dmnt2/hasil-produksi-admin');
                } else {
                    $data['msg']['status'] = 'error';
                    $data['msg']['content'] = 'Data gagal disimpan';
                }
            }
    
            load_view('views/form.php', $data);
        case 'print':
            $sql = 'SELECT * FROM `hasil-produksi-admin` WHERE id = ?';
            $result = $db->query($sql, trim($_GET['id']))->result();
            $data = $result[0];
    
            load_view('views/cetak.php', $data);
    
    }

function cek_action($action)
{
    global $list_action;
    global $db;

    $sql = 'SELECT * FROM `hasil-produksi-admin` WHERE id = ?';
    $result = $db->query($sql, trim($_REQUEST['id']))->result();
    $data = $result[0];
    // echo '<pre>'; print_r ($list_action); die;
    if ($list_action[$action] == 'own') {
        if ($data['id_user_input'] != $_SESSION['user']['id_user']) {
            echo 'Anda tidak diperkenankan mengakses halaman ini';
            die;
        }
    }
}

function set_data($role)
{
    $data_db['id_pesanan'] = $_POST['id_pesanan'];
    $data_db['gambar_desain'] = $_FILES['gambar_desain'];

    return $data_db;
}

function validate_form()
{

    require_once('libraries/form_validation.php');
    $validation = new FormValidation();
    $validation->setRules('id_pesanan', 'ID', 'required');
    $validation->setRules('total_pesanan', 'Total Harga Pesanan', 'required');
    $validation->setRules('total_ongkir', 'Total Ongkir', 'trim|required');
    $validation->setRules('total_pembayaran', 'Total Pembayaran', 'trim|required');
    $validation->setRules('bukti_pembayaran', 'Upload Bukti Pembayaran', 'required');
    $validation->setRules('kategori_paket', 'Kategori Paket', 'required');

    $validation->validate();
    $form_errors = $validation->getMessage();

    return $form_errors;
}