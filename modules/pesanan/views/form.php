<div class="card">
	<div class="card-header">
		<h5 class="card-title"><?=$title?></h5>
	</div>
	
	<div class="card-body">
		<?php 
			include 'helpers/html.php';
			echo btn_label(['class' => 'btn btn-success btn-xs',
				'url' => module_url() . '?action=add',
				'icon' => 'fa fa-plus',
				'label' => 'Tambah Data'
			]);
			
			echo btn_label(['class' => 'btn btn-light btn-xs',
				'url' => module_url(),
				'icon' => 'fa fa-arrow-circle-left',
				'label' => $app_module['judul_module']
			]);
		?>
		<hr/>
		<form method="post" action="" class="form-horizontal" enctype="multipart/form-data">
			<div class="tab-content" id="myTabContent">
				<div class="form-group row">
					<label class="col-sm-3 col-md-2 col-lg-3 col-xl-2 col-form-label">ID</label>
					<div class="col-sm-5">
						<input class="form-control" type="text" name="id_pesanan" value="<?=set_value('id_pesanan', @$id_pesanan)?>" required="required"/>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-3 col-md-2 col-lg-3 col-xl-2 col-form-label">Nama Lengkap</label>
					<div class="col-sm-5">
						<input class="form-control" type="text" name="nama_lengkap" value="<?=set_value('nama_lengkap', @$nama_lengkap)?>" required="required"/>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-3 col-md-2 col-lg-3 col-xl-2 col-form-label">Alamat Lengkap</label>
					<div class="col-sm-5">
						<textarea name="alamat_lengkap" class="form-control"><?=set_value('alamat_lengkap', @$alamat_lengkap)?></textarea>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-3 col-md-2 col-lg-3 col-xl-2 col-form-label">Kecamatan</label>
					<div class="col-sm-5">
						<input class="form-control" type="text" name="kecamatan" value="<?=set_value('kecamatan', @$kecamatan)?>"/>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-3 col-md-2 col-lg-3 col-xl-2 col-form-label">Kabupaten/Kota</label>
					<div class="col-sm-5">
						<input class="form-control" type="text" name="kabupaten" value="<?=set_value('kabupaten', @$kabupaten)?>" required="required"/>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-3 col-md-2 col-lg-3 col-xl-2 col-form-label">Provinsi</label>
					<div class="col-sm-5">
						<input class="form-control" type="text" name="provinsi" value="<?=set_value('provinsi', @$provinsi)?>" required="required"/>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-3 col-md-2 col-lg-3 col-xl-2 col-form-label">No. HP</label>
					<div class="col-sm-5">
						<input class="form-control" type="text" name="no_hp" value="<?=set_value('no_hp', @$no_hp)?>" required="required"/>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-3 col-md-2 col-lg-3 col-xl-2 col-form-label">Nama Tim</label>
					<div class="col-sm-5">
						<input class="form-control" type="text" name="nama_tim" value="<?=set_value('nama_tim', @$nama_tim)?>" required="required"/>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-3 col-md-2 col-lg-3 col-xl-2 col-form-label">Detail Pesanan</label>
					<div class="col-sm-5">
						<textarea name="detail_pesanan" class="form-control"><?=set_value('detail_pesanan', @$detail_pesanan)?></textarea>
						<small class="form-text text-muted">Note : Jumlah Pesanan, Lengan Panjang, Lengan Pendek, Size</small>
					</div>
				</div>
				<div class="form-group row mb-0">
					<div class="col-sm-5">
						<button type="submit" name="submit" value="submit" class="btn btn-primary">Submit</button>
						<input type="hidden" name="id" value="<?=@$_GET['id']?>"/>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>