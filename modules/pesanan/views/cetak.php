<div class="card">
	<div class="card-header">
		<h5 class="card-title">Detail Pesanan</h5>
	</div>
	
	<div class="card-body">
		<?php 
		if (!$data) {
			show_message('Data tidak ditemukan', 'error', false);
		} else {
			if (!empty($msg)) {
				show_alert($msg);
			}
			?>
			<p>ID : <?= $data['id']; ?></p>
			<p>Nama Lengkap : <?= $data['nama_lengkap']; ?></p>
			<p>Alamat Lengkap : <?= $data['alamat_lengkap']; ?></p>
			<p>Kecamatan : <?= $data['kecamatan']; ?></p>
			<p>Kabupaten/Kota : <?= $data['kabupaten']; ?></p>
			<p>Provinsi : <?= $data['provinsi']; ?></p>
			<p>Nomor HP : <?= $data['no_hp']; ?></p>
			<p>Nama Tim : <?= $data['nama_tim']; ?></p>
			<p>Detail Pesanan : <?= $data['detail_pesanan']; ?></p>
			<?php
		} ?>

		<div class="row">
			<div class="col text-center">
				<?php 
					include 'helpers/html.php';
					
					echo btn_label(['class' => 'btn btn-primary btn-xs',
						'url' => module_url(),
						'icon' => 'fa fa-arrow-circle-left',
						'label' => 'Kembali'
					]);
				?>
			</div>
		</div>
	</div>
</div>